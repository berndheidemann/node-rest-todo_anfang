import { Router, Request, Response, NextFunction } from 'express';
import { Todo } from '../model/Todo';
import { TodoDAO } from '../dao/TodoDAO';




export class TodoRouter {
  router: Router

  constructor() {
    this.router = Router();
    this.init();
  }


  async getAll(req: Request, res: Response, next: NextFunction) {
    let todos = await TodoDAO.getAll();
    res.send(todos);
  }

  newTodo(req: Request, res: Response, next: NextFunction) {
    if (!req.body.id || !req.body.label) {
      res.status(405).send("label or id missing");
    }
    let todo = new Todo(req.body.id, req.body.label, 0);
    let err = TodoDAO.save(todo);
    if (err) {
      res.status(500).send("could not save");
    } else {
      res.status(201).send("success");
    }
  }

  async delete(req: Request, res: Response, next: NextFunction) {
    console.log(req.params.id);
    console.log(req.query.foo);
  }

  async foo(req: Request, res: Response, next: NextFunction) {
    res.send("foo");
  }


  init() {
    this.router.get('/', this.getAll);
    this.router.post("/", this.newTodo);
    this.router.delete("/:id", this.delete);
    this.router.get('/foo', this.foo);


  }
}

const todoRoutes = new TodoRouter();
export default todoRoutes.router;
