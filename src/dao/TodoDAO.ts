import { Todo } from '../model/Todo';

// var sqlite3 = require('sqlite3').verbose();

const sqlite = require('sqlite-async')


export class TodoDAO {

    static dbFile = 'databaseTodoApp.db3';


    static async getAll(): Promise<Todo[]> {
        let db = await sqlite.open(TodoDAO.dbFile);
        let todos = await db.all("SELECT todo.id, todo.label, todo.done FROM todo");
        let todoList: Todo[] = [];
        todos.forEach(t => {
            todoList.push(new Todo(t.id, t.label, t.done));
        });
        db.close();
        return todoList;
    }

    static async save(todo: Todo) {
        let db;
        try {
            db = await sqlite.open(TodoDAO.dbFile);
            db.run("insert into todo (id, label, done) VALUES (?, ? , ? )", [todo.id, todo.label, todo.done]);
        } catch (e) {
            console.log(e);
            return e;
        } finally {
            db.close();
        }
    }
}